CC = gcc -Wall -c

bin/taller: obj/main.o
	gcc $^ -o $@ -lm

obj/main.o: src/main.c
	$(CC) $^ -o $@

.PHONY: clean
clean:
	rm bin/taller obj/*.o bin/bw_*.png

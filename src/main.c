#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <sys/types.h> 
#include <sys/wait.h>
#include <string.h>

void execute(char * read, char * write){
	int pid;
	int status;

	if ((pid = fork())<0){
		perror("fork");
		exit(EXIT_FAILURE);
	}

	if (pid == 0){	
		printf("From: %s To: %s converting... \n", read,write);	
		execl("procesador_png", "procesador_png", read, write, NULL);
		perror("excelc");
		exit(EXIT_FAILURE);
	}
	if (pid >= 1 ){
		wait(&status);
		printf("Estado que retorno: %d\n", WEXITSTATUS(status));
	}
}

int main( int argc, char *argv[]) {
	if(argc <= 1){
		printf("Use: ./programa file1 file2 ...\n");
		return (-1);
	}

	for (int i = 1; i < argc; ++i) {
		char salida[sizeof(argv[i])+3] = "bw_";			
		strcat(salida,argv[i]);					
		execute(argv[i],salida);
	}		
	
}